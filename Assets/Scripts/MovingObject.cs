﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class MovingObject : MonoBehaviour
{
    public Animator animator;                  
    public SpriteRenderer spriteRenderer;

    protected bool invertFlip = false;

    protected NavMeshAgent navAgent;

    protected virtual void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.updateRotation = false;
    }

    protected virtual void Update()
    {
        if (!navAgent.isStopped)
        {
            if (navAgent.velocity.x >= 0)
                spriteRenderer.flipX = invertFlip;
            else
                spriteRenderer.flipX = !invertFlip;
        }
    }

    protected void Move(Vector3 direction)
    {
        if (direction.x > 0)
            spriteRenderer.flipX = false;
        else if (direction.x < 0)
            spriteRenderer.flipX = true;

        animator.SetBool("Moving", true);

        navAgent.isStopped = true;
        navAgent.Move(direction.normalized * Time.deltaTime * navAgent.speed);
    }

    protected void MoveToPosition(Vector3 position)
    {
        animator.SetBool("Moving", true);
        navAgent.isStopped = false;
        navAgent.destination = position;
    }


}
