﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;        
    public BoardManager BoardScript;          
    public GameObject MainMenuGO;
    public GameObject StatisticsGO;
    public GameObject GameMenuGO;

    public InputField PlayerName;
    public Text GameCoins;
    public Text GameTimer;

    public bool gameStarted = false;

    private float gameTimer = 0f;
    private string playerName = "Player";
    private StatisticsController statsController;

    public void StartGame()
    {
        gameTimer = 0f;
        BoardScript.SetupLevel();
        gameStarted = true;

        ShowGameMenu();

        playerName = PlayerName.text;
    }

    public void GameOver(int coinsCount, bool escaped)
    {
        gameStarted = false;
        SaveDataToXML(coinsCount, escaped);

        ShowMainMenu();
    }

    public void ShowCurrentCoins(int count)
    {
        GameCoins.text = count.ToString();
    }

    public void ShowTimer()
    {
        GameTimer.text = (gameTimer/60f).ToString("#0#:")+(gameTimer%60).ToString("00");
    }

    public void ShowStatisics()
    {
        MainMenuGO.SetActive(false);
        StatisticsGO.SetActive(true);
        GameMenuGO.SetActive(false);
    }

    public void ShowMainMenu()
    {
        MainMenuGO.SetActive(true);
        StatisticsGO.SetActive(false);
        GameMenuGO.SetActive(false);
    }

    public void ShowGameMenu()
    {
        MainMenuGO.SetActive(false);
        StatisticsGO.SetActive(false);
        GameMenuGO.SetActive(true);
    }

    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        ShowMainMenu();

        statsController = StatisticsGO.GetComponent<StatisticsController>();
        statsController.Init();
    }

    private void Update()
    {
        if (gameStarted) {
            gameTimer += Time.deltaTime;
            ShowTimer();
        }
    }

    private void SaveDataToXML(int coinsCount, bool escaped)
    {
        statsController.SaveToXML(new StatisticsController.StatItem(playerName, coinsCount, gameTimer, escaped));
    }

}

