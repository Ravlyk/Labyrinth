﻿using UnityEngine;

public class Enemy : MovingObject
{
    public enum EnemyType {
        Zombie, Mummy,
    }

    public EnemyType Type = EnemyType.Zombie;

    private Transform target;
    private float range = 3f;
    private bool isHaunt = false;

    protected override void Start()
    {
        base.Start();

        target = GameObject.FindGameObjectWithTag("Player").transform;
        invertFlip = true;
    }

    Vector2 randomCircle;
    protected override void Update()
    {
        base.Update();
        
        if (!isHaunt)
        {
            if (navAgent.pathPending || (navAgent.remainingDistance > navAgent.stoppingDistance))
                return;

            randomCircle = Random.insideUnitCircle;
            MoveToPosition(transform.position + range * new Vector3(randomCircle.x, 0f, randomCircle.y));
        }
        else {
            MoveToPosition(target.position);
        }
    }

    public void Haunt()
    {
        isHaunt = true;
    }

    public void SpeedUp()
    {
        navAgent.speed *= 1.05f;
    }
}
