﻿using UnityEngine.UI;
using UnityEngine;

public class StatisticsItemView : MonoBehaviour {

    public Text Name;
    public Text Coins;
    public Text Time;
    public Text Date;
    public Text LooseType;

}
