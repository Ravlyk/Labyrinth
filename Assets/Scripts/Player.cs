﻿using UnityEngine;

public class Player : MovingObject
{
    [HideInInspector]
    public BoardManager boardManager;

    private RaycastHit m_HitInfo = new RaycastHit();

    private int horizontal = 0;  
    private int vertical = 0;      

    protected override void Update()
    {
        //Player controls
        horizontal = (int)(Input.GetAxisRaw("Horizontal"));
        vertical = (int)(Input.GetAxisRaw("Vertical"));

        if (Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.LeftShift))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out m_HitInfo))
                MoveToPosition(m_HitInfo.point);
        }

        if (horizontal != 0 || vertical != 0)
        {
            Move(new Vector3(horizontal, 0f, vertical));
        }else if(navAgent.isStopped || (navAgent.remainingDistance <= navAgent.stoppingDistance && !navAgent.pathPending))
            animator.SetBool("Moving", false);

        base.Update();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            other.gameObject.SetActive(false);
            boardManager.AddCoin();
        }

        else if (other.tag == "Enemy")
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            if (enemy.Type == Enemy.EnemyType.Mummy)
                boardManager.LooseCoins();
            boardManager.GameOver();
        }

    }



}

