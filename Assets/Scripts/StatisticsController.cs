﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System;

public class StatisticsController : MonoBehaviour {

    public GameObject StatObjectTemplate;

    public GameObject StatisticsContainerGO;

    private const string fileName = "GameStats.XML";
    private StatsContainer statsContainer;

    private string formatDayString = "MM-dd";
    private string formatTimeString = "HH:mm";

    public class StatItem
    {
        public DateTime GameDateTime;
        public string PlayerName;
        public int CoinsCount;
        public float GameTime;
        public bool Escaped;

        public StatItem(string PlayerName, int CoinsCount, float GameTime, bool Escaped)
        {
            this.PlayerName = PlayerName;
            this.CoinsCount = CoinsCount;
            this.GameTime = GameTime;
            this.Escaped = Escaped;
            this.GameDateTime = DateTime.Now;
        }

        public StatItem() {
            this.PlayerName = "Player";
            this.CoinsCount = 0;
            this.GameTime = 0f;
            this.Escaped = true;
            this.GameDateTime = DateTime.Now;
        }
    }

    [XmlRoot("StaticsContainer")]
    public class StatsContainer
    {
        [XmlArray("FinishedGames")]
        [XmlArrayItem("GameStats")]
        public List<StatItem> items = new List<StatItem>();

        public StatsContainer() { }
    }

    public void SaveToXML(StatItem item)
    {
        statsContainer.items.Add(item);

        XmlSerializer serializer = new XmlSerializer(typeof(StatsContainer));
        using (var stream = new FileStream(Path.Combine(Application.persistentDataPath, fileName), FileMode.Create))
        {
            serializer.Serialize(stream, statsContainer);
        }
    }

    public void ShowStatistics()
    {
        foreach (Transform child in StatisticsContainerGO.transform)
        {
            Destroy(child.gameObject);
        }

        GameObject tmpGO;
        StatisticsItemView tmpView;
        RectTransform containerRectTransform = StatisticsContainerGO.GetComponent<RectTransform>();
        statsContainer.items.Sort(new Comparison<StatItem>( (x, y) => -DateTime.Compare(x.GameDateTime , y.GameDateTime)) );
        foreach (StatItem sItem in statsContainer.items)
        {
            tmpGO = Instantiate(StatObjectTemplate, StatisticsContainerGO.transform) as GameObject;
            tmpView = tmpGO.GetComponent<StatisticsItemView>();
            tmpView.Name.text = sItem.PlayerName;
            tmpView.Coins.text = sItem.CoinsCount.ToString();
            tmpView.Time.text = (sItem.GameTime / 60f).ToString("#0#:") + (sItem.GameTime % 60).ToString("00");
            tmpView.Date.text = sItem.GameDateTime.ToString(formatDayString) + '\n' + sItem.GameDateTime.ToString(formatTimeString);
            if (sItem.Escaped)
                tmpView.LooseType.text = "Escaped";
            else
                tmpView.LooseType.text = "Died";
        }
    }

    public void Init()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(StatsContainer));
        try
        {
            if (File.Exists(Path.Combine(Application.persistentDataPath, fileName)))
            {
                var stream = new FileStream(Path.Combine(Application.persistentDataPath, fileName), FileMode.Open);
                statsContainer = serializer.Deserialize(stream) as StatsContainer;
            }
        }
        finally {
            if (statsContainer == null)
                statsContainer = new StatsContainer();
        }
    }

    private void OnEnable()
    {
        ShowStatistics();
    }
    
}
